import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from "reactstrap";
import Navigation from "./components/Navigation";

import CuentaModel from "./model/Cuenta";
import Home from "./components/Home";
import ModificaCuenta from "./components/cuentas/ModificaCuentas";
import ListaAlumnos from "./components/cuentas/ListaCuentas";
import EliminaCuenta from "./components/cuentas/EliminaCuentas";
import NuevaCuenta from "./components/cuentas/NuevaCuenta";

function App() {
  const [cuentas, setAlumnos] = useState([]);
  async function loadData() {
    let cuents = await CuentaModel.getAll();

    setAlumnos(cuents);
  }

  //alumnos
  async function actualizaCuenta(cuenta) {
    let x = await CuentaModel.update(cuenta);
    loadData();
  }

  async function nuevaCuenta(cuenta) {
    let x = await CuentaModel.add(cuenta);
    loadData();
  }

  async function eliminaCuenta(cuenta) {
    let x = await CuentaModel.remove(cuenta);
    loadData();
  }

  useEffect(() => {
    loadData();
  }, []);

  return (
    <BrowserRouter>
      <Navigation />
      <Container>
        <Switch>
          <Route exact path="/" render={() => <Home />} />
          <Route
            path="/cuentas/list"
            render={() => <ListaAlumnos cuentas={cuentas} />}
          />
          <Route
            path="/cuentas/nuevo"
            render={() => <NuevaCuenta nuevaCuenta={nuevaCuenta} />}
          />
          <Route
            path="/cuentas/modifica/:id"
            render={(props) => (
              <ModificaCuenta
                id={props.match.params.id}
                cuentas={cuentas}
                actualizaCuenta={actualizaCuenta}
              />
            )}
          />
          <Route
            path="/cuentas/elimina/:id"
            render={(props) => (
              <EliminaCuenta
                id={props.match.params.id}
                cuentas={cuentas}
                eliminaCuenta={eliminaCuenta}
              />
            )}
          />
          <Route render={() => <h1>Not found</h1>} />
        </Switch>
      </Container>
    </BrowserRouter>
  );
}

//props.match.params.id

export default App;
