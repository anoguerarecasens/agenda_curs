import React from "react";
import { Container, Row, Col } from "reactstrap";
import Logo from "../images/logo512.png";

function Home() {
  return (
    <>
      <Container>
        <Row>
          <Col style={{ textAlign: "center" }}>
            <img src={Logo} alt="logo" />
            <h1>Academy Web for API Callss</h1>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Home;
