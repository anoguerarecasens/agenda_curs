import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { Row, Col } from "reactstrap";
import Moment from "moment";

const ModificaCuenta = (props) => {
  Moment.locale("en");

  const id = props.id * 1; //convertiremos a int
  const cuenta = props.cuentas.find((el) => el.idCuentas === id);

  const [idCuentas, setCuenta] = useState(cuenta.idCuentas);
  const [iban, setIban] = useState(cuenta.iban);
  const [saldo, setSaldo] = useState(cuenta.saldo);
  const [fechaCreacion, setFechaCreacion] = useState(
    Moment(cuenta.fechaCreacion).format("YYYY-MM-DD 00:00:00")
  );
  const [fechaCierre, setFechaCierre] = useState(cuenta.fechaCierre);
  const [entidad, setEntidad] = useState(cuenta.entidad);
  const [descripcion, setDescripcion] = useState(cuenta.descripcion);
  const [volver, setVolver] = useState(false);

  /*
  idCuentas: 1,
iban: "ES01 2100 5678 9012 3456 7890",
saldo: 0,
fechaCreacion: "2015-12-31T23:00:00.000Z",
fechaCierre: null,
entidad: "Sabadell",
descripcion: "Día a Día"
   */

  //metodo que se activa al enviar el formulario
  function guardar(e) {
    e.preventDefault();
    //validación de los datos

    props.actualizaCuenta({
      idCuentas,
      iban,
      saldo,
      fechaCreacion,
      fechaCierre,
      entidad,
      descripcion,
    });
    setVolver(true);
  }

  function cancelar() {
    setVolver(true);
  }

  if (volver) {
    return <Redirect to="/cuentas/list" />;
  }

  return (
    <>
      <br />
      <h3>Modifica contacto</h3>
      <Form onSubmit={guardar}>
        <Row>
          <Col xs="6">
            <FormGroup>
              <Label for="ibanInput">Iban</Label>
              <Input
                type="text"
                id="ibanInput"
                value={iban}
                onChange={(e) => setIban(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="saldoInput">Saldo</Label>
              <Input
                type="text"
                id="saldoInput"
                value={saldo}
                onChange={(e) => setSaldo(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="entidadInput">Entidad</Label>
              <Input
                type="text"
                id="entidadInput"
                value={entidad}
                onChange={(e) => setEntidad(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="descripcionInput">Descripcion</Label>
              <Input
                type="text"
                id="descripcionInput"
                value={descripcion}
                onChange={(e) => setDescripcion(e.target.value)}
              />
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col>
            <Button type="submit" color="primary">
              Guardar
            </Button>{" "}
            <Button type="button" onClick={cancelar} color="danger">
              Cancelar
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default ModificaCuenta;
