import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { Button } from "reactstrap";

const EliminaCuenta = (props) => {
  const id = props.id * 1;
  const cuenta = props.cuentas.find((el) => el.idCuentas === id);
  const [volver, setVolver] = useState(false);

  function eliminar() {
    props.eliminaCuenta(id);
    setVolver(true);
  }

  function cancelar() {
    setVolver(true);
  }

  if (volver === true) {
    return <Redirect to="/cuentas/list" />;
  }

  return (
    <>
      <br />
      <h3>Desea eliminar a {cuenta.nombre}?</h3>
      <br />
      <Button onClick={eliminar} color="danger">
        Sí
      </Button>{" "}
      <Button onClick={cancelar} color="success">
        No
      </Button>
    </>
  );
};

export default EliminaCuenta;
