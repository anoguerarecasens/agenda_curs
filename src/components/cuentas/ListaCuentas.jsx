import React from "react";

import { Table } from "reactstrap";
import { Link } from "react-router-dom";
import Moment from "moment";

const ListaAlumnos = (props) => {
  Moment.locale("en");

  let filas = props.cuentas.map((cuenta) => {
    return (
      <tr key={cuenta.idCuentas}>
        <td>{cuenta.idCuentas}</td>
        <td>{cuenta.iban}</td>
        <td>{cuenta.saldo}</td>
        <td>{Moment(cuenta.fechaCreacion).format("YYYY-MM-DD 00:00:00")}</td>
        <td>{cuenta.entidad}</td>
        <td>{cuenta.descripcion}</td>
        <td>
          <Link to={"/cuentas/modifica/" + cuenta.idCuentas}>
            <i className="fa fa-edit fa-2x"></i>
          </Link>
        </td>
        <td>
          <Link to={"/cuentas/elimina/" + cuenta.idCuentas}>
            <i style={{ color: "red" }} className="fa fa-trash fa-2x "></i>
          </Link>
        </td>
      </tr>
    );
  });

  return (
    <>
      <br />
      <h3>Alumnos</h3>
      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>Iban</th>
            <th>Saldo</th>
            <th>Fecha Creación</th>
            <th>Entidad</th>
            <th>Descripción</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>{filas}</tbody>
      </Table>
      <br />
      <Link className="btn btn-success" to="/cuentas/nuevo">
        Nuevo
      </Link>
    </>
  );
};

export default ListaAlumnos;
