import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { Row, Col } from "reactstrap";

const NuevaCuenta = (props) => {
  const [idCuentas, setCuenta] = useState("");
  const [iban, setIban] = useState("");
  const [saldo, setSaldo] = useState("");
  const [fechaCreacion, setFechaCreacion] = useState("");
  const [fechaCierre, setFechaCierre] = useState(null);
  const [entidad, setEntidad] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [volver, setVolver] = useState(false);

  function guardar(e) {
    e.preventDefault();

    props.nuevaCuenta({
      idCuentas,
      iban,
      saldo,
      fechaCreacion,
      fechaCierre,
      entidad,
      descripcion,
    });
    setVolver(true);
  }

  function cancelar() {
    setVolver(true);
  }

  if (volver === true) {
    return <Redirect to="/contactos/lista" />;
  }

  return (
    <>
      <br />
      <h3>Modifica contacto</h3>
      <Form onSubmit={guardar}>
        <Row>
          <Col xs="6">
            <FormGroup>
              <Label for="ibanInput">Iban</Label>
              <Input
                type="text"
                id="ibanInput"
                value={iban}
                onChange={(e) => setIban(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="saldoInput">Saldo</Label>
              <Input
                type="text"
                id="saldoInput"
                value={saldo}
                onChange={(e) => setSaldo(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="entidadInput">Entidad</Label>
              <Input
                type="text"
                id="entidadInput"
                value={entidad}
                onChange={(e) => setEntidad(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="descripcionInput">Descripcion</Label>
              <Input
                type="text"
                id="descripcionInput"
                value={descripcion}
                onChange={(e) => setDescripcion(e.target.value)}
              />
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col>
            <Button type="submit" color="primary">
              Guardar
            </Button>{" "}
            <Button type="button" onClick={cancelar} color="danger">
              Cancelar
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default NuevaCuenta;
