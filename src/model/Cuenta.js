const API = "http://localhost:3000/api/cuentas/";
const HOST = "localhost";

const HEADERS = new Headers({
  Accept: "application/json",
  "Content-Type": "application/json",
  Host: HOST,
});

async function getAll() {
  const opcionesFetch = {
    method: "GET",
    headers: HEADERS,
  };

  const response = await fetch(API, opcionesFetch);
  if (response.ok) {
    const retorn = await response.json();
    return retorn;
  } else return;
}

async function add(datos) {
  const opcionesFetch = {
    method: "POST",
    headers: HEADERS,
    body: JSON.stringify(datos),
  };

  const response = await fetch(API, opcionesFetch);
  return response.ok; //true or false
}

async function update(datos) {
  const opcionesFetch = {
    method: "PATCH",
    headers: HEADERS,
    body: JSON.stringify(datos),
  };

  const response = await fetch(API + datos.idCuentas, opcionesFetch);
  return response.ok; //true or false
}

async function remove(idEliminar) {
  const opcionesFetch = {
    method: "DELETE",
    headers: HEADERS,
  };

  const response = await fetch(API + idEliminar, opcionesFetch);
  return response.ok; //true or flase
}

const Alumno = { getAll, add, update, remove };

export default Alumno;
